<section class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="titulos">Newsletter</h2>
                <hr>
                <p>Cadastre-se em nossa newsletter e fique por dentro das novidades</p>
                <div class="get-news-letter-mail">
                  <?php if (isset($status) && $status) : ?>
                      <div class="alert alert-primary" role="alert">
                        <?php echo nl2br($status); ?>
                      </div>
                  <?php endif;?>
                    <form id="form-news-letter" class="signup form-inline" action="<?php echo get_the_permalink(); ?>#form-news-letter" method="post"  enctype="multipart/form-data">
                        <input <?php if(FIELD_NAME == FALSE){ ?>style="display:none"<?php } ?> name="news_letter_name" type="text" class="col-md-4 col-sm-12 col-xs-12" value="<?php echo isset($_POST['news_letter_name']) ? $_POST['news_letter_name'] : ''; ?>" placeholder="<?php echo isset($atts['name']) ? $atts['name'] : 'Nome:'; ?>">
                        <input <?php if(FIELD_EMAIL == FALSE){ ?>style="display:none"<?php } ?> name="news_letter_mail" type="email" class="col-md-4 col-sm-12 col-xs-12" value="<?php echo isset($_POST['news_letter_mail']) ? $_POST['news_letter_mail'] : ''; ?>" placeholder="<?php echo isset($atts['mail']) ? $atts['mail'] : 'Email:'; ?>">
                        <input <?php if(FIELD_LIST == FALSE){ ?>style="display:none"<?php } ?> name="news_letter_list" type="text" class="col-md-4 col-sm-12 col-xs-12" value="<?php echo isset($_POST['news_letter_list']) ? $_POST['news_letter_list'] : ''; ?>" placeholder="<?php echo isset($atts['list']) ? $atts['list'] : 'Área de Interesse:'; ?>">
                        <button type="submit" class="theme_button"><?php echo isset($atts['buttom']) ? $atts['buttom'] : 'Cadastrar'; ?></button>
                        <div class="response"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>