<?php get_header() ?>

    <!-- Slide -->
<?php get_template_part('components/banner/banner'); ?>

    <!-- Diferenciais -->
<?php get_template_part('components/diferenciais/diferenciais'); ?>

    <!-- Chamada -->
<?php get_template_part('components/chamada/chamada'); ?>

    <!-- Seções Alternadas -->
<?php get_template_part('components/secoes-alternadas/secoes-alternadas'); ?>

    <!-- Portfólio -->
<?php get_template_part('components/portfolio/portfolio'); ?>

    <!-- Como Funciona -->
<?php get_template_part('components/como-funciona/como-funciona'); ?>

    <!-- Depoimentos -->
<?php get_template_part('components/depoimentos/depoimentos'); ?>

    <!-- Calculadora -->
<?php get_template_part('components/calculadora/calculadora'); ?>

<?php get_footer() ?>