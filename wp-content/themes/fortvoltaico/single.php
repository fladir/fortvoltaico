<?php get_header(); ?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Post -->
    <section id="blog">
        <article>
            <div class="container">
                <div class="row">
<div class="col-md-1 sidebar-compartilhar">
    <ul class="compartilhar-post">
        <li class="facebook">
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink() ?>">
                <i class="fab fa-facebook"></i>
            </a>
        </li>
        <li class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=&text=<?php echo get_the_permalink() ?>">
                <i class="fab fa-whatsapp"></i>
            </a>
        </li>
        <li class="email">
            <a href="mailto:?&subject=&body=<?php echo get_the_permalink() ?>">
                <i class="fas fa-envelope"></i>
            </a>
        </li>
        <li class="twitter">
            <a href="https://twitter.com/home?status=<?php echo get_the_permalink() ?>">
                <i class="fab fa-twitter"></i>
            </a>
        </li>
        <li class="linkedin">
            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink() ?>&title=&summary=&source=">
                <i class="fab fa-linkedin"></i>
            </a>
        </li>
    </ul>

</div>
                    <div class="col-md-8  content-post">
                        <div class="img-post-principal">
                            <h1><?php the_title(); ?></h1>
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('blog-destaque', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                            <?php endif; ?>
                        </div>

                        <?php the_content(); ?>

                    </div>
                    <div class="col-md-3 sidebar-single-post">
                        <?php dynamic_sidebar('single_post_sidebar'); ?>
                    </div>

                </div>
            </div>
        </article>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <hr class="hr-tracejado">
            </div>
        </div>
    </div>
    <section id="rodape-posts">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="mb-4">
                        Posts Relacionados
                    </h2>
                </div>
                <div class="col-md-8 posts-relacionados">

                    <?php
                    //for use in the loop, list 5 post titles related to first tag on current post
                    $tags = wp_get_post_tags($post->ID);
                    if ($tags) {
                        $first_tag = $tags[0]->term_id;
                        $args = array(
                            'tag__in' => array($first_tag),
                            'post__not_in' => array($post->ID),
                            'posts_per_page' => 2,
                            'caller_get_posts' => 1
                        );
                        ?>
                        <div class="row mb-5">
                        <?php $my_query = new WP_Query($args);
                        if ($my_query->have_posts()) {
                            while ($my_query->have_posts()) : $my_query->the_post(); ?>

                                <div class="col-md-6">
                                    <a class="link-imagem-recents-post" href="<?php echo get_the_permalink() ?>">
                                        <figure>
                                            <?php the_post_thumbnail('img_post_list', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                        </figure>
                                    </a>


                                    <a class="link-titulo-recents-post" href="<?php echo get_the_permalink() ?>">
                                        <h2 class="text-left py-3"><?php echo get_the_title() ?></h2>
                                    </a>
                                    <div class="button_su">
                                        <span class="su_button_circle"></span>
                                        <a href="<?php echo get_the_permalink() ?>" class="btn button_su_inner primario">
                                        <span class="button_text_container">Leia
                                    Mais</span>
                                        </a>
                                    </div>
                                </div>


                            <?php
                            endwhile; ?>
                            </div>
                            <?php
                        }
                        wp_reset_query();
                    }
                    ?>
                </div>
                <div class="col-md-4">
                    <?php dynamic_sidebar('rodape_posts'); ?>

                </div>
            </div>
        </div>
    </section>

<?php get_template_part('/components/newsletter/newsletter'); ?>

<?php get_footer(); ?>