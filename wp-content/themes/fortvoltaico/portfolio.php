<?php
/* Template Name: Portfólio */
get_header();

$args = array(
    'nopaging' => false,
    'post_type' => 'portfolio',
    'posts_per_page' => 8,
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);

?>

    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="solucoes">
        <div class="container">
            <div class="row">
                <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                    <div class="col-md-6 mb-4">
                        <div class="conteudo-solucao h-100">
                            <div class="icone-titulo">
                                <?php the_post_thumbnail('icone_certificado', array('class' => 'img-archive-portfolio', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                <h4 class="text-white"><?php the_title() ?></h4>
                            </div>
                            <?php the_excerpt(); ?>
                            <a href="<?php echo get_the_permalink() ?>"
                               class="btn btn-primario dark-hover text-center mt-3">Conheça</a>
                        </div>
                    </div>

                <?php endwhile; endif;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </section>

    <!-- Nosso Processo -->
<?php  get_template_part('components/index/nosso-processo'); ?>

    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

    <!-- Rastreamento -->
<?php  get_template_part('components/rastreamento/rastreamento'); ?>
<?php get_footer(); ?>