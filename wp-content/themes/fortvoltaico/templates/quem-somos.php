<?php
/* Template Name: Quem Somos */
get_header();
$grupo_caixas_coloridas = get_field('grupo_caixas_coloridas')
?>

    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Institucional -->
<?php  get_template_part('components/quem-somos/institucional'); ?>

    <!-- Caixas Coloridas -->
<?php  get_template_part('components/quem-somos/caixas-coloridas'); ?>

    <!-- Clientes -->
<?php get_template_part('components/clientes/clientes'); ?>

    <!-- Nossos Numeros -->
<?php get_template_part('components/nossos-numeros/nossos-numeros'); ?>

    <!-- Equipe -->
<?php get_template_part('components/quem-somos/equipe'); ?>

    <!-- Depoimentos -->
<?php  get_template_part('components/depoimentos/depoimentos'); ?>

    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

    <!-- Portfolio -->
<?php get_template_part('components/index/portfolio'); ?>

    <!-- Newsletter -->
<?php get_template_part('components/newsletter/newsletter'); ?>


<?php get_footer(); ?>