<?php
$grupoFooter = get_fields('options')['grupo_footer'];
$grupoCertificados = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_certificados'];
$certificacoes = $grupoCertificados['certificados'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];

?>

<!-- Footer -->
<footer class="font-small text-white">
    <div class="container text-center text-sm-left wrapper-footer">
        <div class="row ">

            <div class="col-md-5">
                <a class="logo-footer mb-3" href="<?php bloginfo('url'); ?>">
                    <?php echo wp_get_attachment_image(get_field('grupo_footer', 'options')['logo_footer'], 'footer_logo'); ?>
                </a>
            </div>
            <div class="col-md-7">
                <p class="texto-footer fw-semi-bold">
                    <?php echo $grupoFooter['Texto_footer'] ?>
                </p>

            </div>

        </div>
        <div class="row align-items-center justify-content-around contatos-footer mt-4">
            <div class="col-6 text-left container">
                <?php foreach ($telefones as $telefone) : ?>
                    <span class="telefone mr-3">
                                        <span class="icon-wrapper">
                                            <i class="fas fa-phone-alt mr-2 "></i>
                                        </span>
                                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                                    <?php echo $telefone['numero_telefone']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>

                <?php foreach ($whatsapps as $whatsapp) : ?>
                    <span class="whatsapp mr-3">
                                        <span class="icon-wrapper">
                                            <i class="fab fa-whatsapp mr-2"></i>
                                        </span>
                                    <a href="https://api.whatsapp.com/send?phone=55<?php echo $whatsapp['link_whatsapp']; ?>&text=Ola,%20tudo%20bem?"
                                       target="_blank">
                                    <?php echo $whatsapp['numero_whatsapp']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>

                <?php foreach ($emails as $email) : ?>
                    <span class="email mr-3">
                                        <span class="icon-wrapper">
                                            <i class="fas fa-envelope mr-2"></i>
                                        </span>
                                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                                    <?php echo $email['endereco_email']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>
            </div>

            <div class="col-6 text-right container">
                <?php foreach ($redesSociais as $redesSocial) : ?>
                    <span class="redes-sociais mr-1">
                                    <a href="mailto:<?php echo $redesSocial['link_social']; ?>" target="_blank"
                                       title="<?php echo $redesSocial['nome_rede_social']; ?>">
                                    <i class="<?php echo $redesSocial['icone_social']; ?>"></i>
                                    </a>
                                </span>
                <?php endforeach; ?>
            </div>
        </div>


    </div>

    <!-- Copyright -->
    <?php get_template_part('/components/footer/cia-logo-footer'); ?>
    <!-- Copyright -->

</footer>
<div class="mobile-menu-overlay"></div>
<!-- Footer -->
<?php if (wp_is_mobile()) : ?>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            // Adicionando o dropdown no Menu do topo apenas no mobile
            const tela = window.matchMedia("(max-width: 991px)").matches;
            if (tela) {
                const menu = document.querySelectorAll('.mobile-menu li.menu-item-has-children');
                menu.forEach((item) => {
                    let id = item.getAttribute('id');
                    let link = item.querySelector('a[id^=menu-item-dropdown-476');
                    link.setAttribute('href', '#');
                    link.addEventListener('click', (event) => {
                        event.preventDefault();
                        let dropdown = item.querySelector('ul[aria-labelledby=menu-item-dropdown-${number[2]}]');
                        dropdown.classList.toggle('show')
                    })
                })
            }
        })
    </script>
<?php endif; ?>
<?php $botaoLateral = get_field('grupo_informacoes_para_contato', 'options')['link_botao_lateral']; ?>
<div class="botao-lateral-wrapper">
    <div class="botao-lateral">
        <a title="Contato" rel="nofollow noreferrer noopener external" data-toggle="modal" data-target="#exampleModal">
            <i class="fas fa-envelope-open"></i>
        </a>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-primario fw-bold text-center w-100">Fale Conosco</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="wrapper-form-modal bg-secundario p-5">
                <?php get_template_part('components/formularios/contact'); ?>
            </div>
        </div>
    </div>
</div>
<?php wp_footer() ?>

</body>

</html>