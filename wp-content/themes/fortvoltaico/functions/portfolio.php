<?php
add_action('init', 'type_post_portfolio');

function type_post_portfolio() {
    $labels = array(
        'name' => _x('Portfólio', 'post type general name'),
        'singular_name' => _x('Portfólio', 'post type singular name'),
        'add_new' => _x('Adicionar Novo Portfólio', 'Novo Portfólio'),
        'add_new_item' => __('Novo Portfólio'),
        'edit_item' => __('Editar Portfólio'),
        'new_item' => __('Novo Portfólio'),
        'view_item' => __('Ver Portfólio'),
        'search_items' => __('Procurar Portfólio'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Portfólio'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'register_meta_box_cb' => 'portfolio_meta_box',
        'menu_icon' => 'dashicons-images-alt2',
        'supports' => array('title', 'editor', 'thumbnail'),
    );

    register_post_type( 'portfolio' , $args );
    flush_rewrite_rules();
}
?>