<?php
#DEPOIMENTOS
$args = array(
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    'menu_icon' => 'dashicons-format-chat',
);
$custom_post_type_slide = new pbo_register_custom_post_type('depoimento', 'Depoimento', $args);

#  Meta box depoimentos
$iniciar_meta_box_depoimentos = new pbo_register_meta_box('depoimento', 'Informações do Depoimento', array('depoimento'));

$args = array(
    'label' => 'Função do Cliente',
    'atributos' => array(
        'id' => 'funcaoCliente',
        'placeholder' => 'Digite a função do cliente',
        'name' => 'funcaoCliente',
    )
);

$iniciar_meta_box_depoimentos->add_field_form('text', $args);


