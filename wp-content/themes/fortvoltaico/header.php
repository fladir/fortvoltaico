<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title('|', true, 'right') ?></title>

    <?php if (get_field('grupo_header', 'options')['favicon']): ?>
        <meta content="<?php echo get_field('grupo_header', 'options')['favicon']; ?>" itemprop="image">
        <link href="<?php echo get_field('grupo_header', 'options')['favicon']; ?>" rel="shortcut icon">
    <?php endif; ?>


    <!--/*Início Codigo Analytics*/-->
    <?php if (get_field('analytics', 'options')): ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', '<?php echo get_field('analytics', 'options'); ?>', 'auto');
            ga('send', 'pageview');
        </script>
    <?php endif; ?>
    <!--/*Final Codigo Analytics*/-->


    <!--/*Início Codigo Head*/-->
    <?php if (get_field('code_header', 'options')): ?>
        <?php echo get_field('code_header', 'options'); ?>
    <?php endif; ?>
    <!--/*Final Codigo Head*/-->

    <?php wp_head() ?>

    <style>
        :root {
        <?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_primaria']) : ?> --corPrimaria: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_primaria']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_secundaria']) : ?> --corSecundaria: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_secundaria']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['call_to_action']) : ?> --corTerciaria: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['call_to_action']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['footer']) : ?> --corBody: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['footer']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_titulos']) : ?> --corAuxiliar: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_titulos']; ?>;
        <?php endif; ?>
        }
    </style>
</head>

<body <?php body_class(); ?>>
<a id="voltarTopo"></a>
<section class="menu-wrapper">
    <?php get_template_part('components/header/menu_primario'); ?>
    <?php get_template_part('components/header/menu_secundario'); ?>
</section>

  