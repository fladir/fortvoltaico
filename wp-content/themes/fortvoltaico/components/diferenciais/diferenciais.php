<?php
$diferenciais = get_field('diferenciais');
#echo '<pre>'; print_r($diferenciais); echo '</pre>';
?>
<section id="diferenciais">
    <div class="container">
        <div class="row">
            <?php if($diferenciais) : foreach ($diferenciais as $diferencial) : ?>
            <div class="col-md">
                <img src="<?php print_r($diferencial['icone']['sizes']['diferenciais']) ?>" alt="<?php echo $diferencial['texto']; ?>" class="attachment-diferenciais">
                <h4 class="fw-bold text-center mt-4"><?php echo $diferencial['texto']; ?></h4>
            </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>


