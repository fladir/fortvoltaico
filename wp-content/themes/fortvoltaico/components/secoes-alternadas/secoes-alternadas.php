<?php
$secoesAlternadas = get_field('secoes_alternadas');
#echo '<pre>'; print_r($secoesAlternadas); echo '</pre>';
?>

<section id="secoes-alternadas">
    <div class="container">
        <?php
        if (have_rows('secoes_alternadas')):
            $i = 0;
            while (have_rows('secoes_alternadas')) : the_row();
                $class = $i % 2 ? 'flex-row-reverse' : '';
                $bg = $i % 2 ? 'bg-primario' : 'bg-secundario';
                $after_bg = $i % 2 ? 'afbg-primario' : 'afbg-secundario';
                ?>
                <?php
                $imagem = get_sub_field('imagem');
                $titulo = get_sub_field('titulo');
                $texto = get_sub_field('texto');
                ?>
                <div class="row <?php echo $class; ?>">
                    <div class="col-md-1 d-flex justify-content-center align-items-center">
                        <div class="numero-secao d-flex justify-content-center align-items-center <?php echo $bg ?> fw-bold text-white">
                            0<?php echo $i + 1 ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h3 class="fw-bold"><?php echo $titulo ?></h3>
                        <?php echo $texto ?>

                    </div>
                    <div class="col-md-7">
                        <div class="wrapper-attachment-secoes-aternadas  d-flex justify-content-center align-items-center <?php echo $after_bg ?> ">
                            <img src="<?php print_r($imagem['sizes']['secoes_alternadas']) ?>"
                                 alt="<?php echo $titulo ?>" class="attachment-secoes-aternadas">
                        </div>
                    </div>
                </div>
                <?php
                $i++;
            endwhile;
        endif;
        ?>
    </div>
</section>

