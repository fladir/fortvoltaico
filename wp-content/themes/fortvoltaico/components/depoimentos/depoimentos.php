<?php
$depoimentos = get_field('depoimentos');
?>

<section id="depoimentos" class="bg-secundario">
    <img src="<?php echo get_template_directory_uri() . '/assets/img/circulo-depoimento.png'; ?>" class="circulo-depoimentos-antes d-none d-md-block"/>

    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-md-6 d-flex flex-column flex-md-row align-items-center align-items-md-start mt-5 pt-5">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/aspas-depoimentos.png'; ?>" class="aspas-depoimentos"/>

                <h2 class="text-white fw-bold">
                    Depoi<br>
                    men <br>
                    tos
                </h2>
            </div>
            <div class="col-md-6">
                <div class="owl-carousel owl-theme depoimento-carousel">
                    <?php if ($depoimentos) : foreach ($depoimentos as $depoimento) : ?>
                        <div class="item">
                            <img src="<?php print_r($depoimento['foto_do_perfil']['sizes']['depoimentos']) ?>"
                                 alt="<?php echo $depoimento['nome_do_cliente']; ?>" class="attachment-depoimento">
                            <div class="wrapper-depoimento-content text-center">
                                <p class="fw-regular depoimento">
                                    <?php echo $depoimento['depoimento']; ?>
                                </p>
                                <h5 class="fw-semi-bold mb-0"><?php echo $depoimento['nome_do_cliente']; ?></h5>
                                <p class="fw-regular"><?php echo $depoimento['cargo_do_cliente']; ?></p>
                            </div>

                        </div>
                    <?php endforeach; endif; ?>
                </div>
            </div>
        </div>
    </div>
    <img src="<?php echo get_template_directory_uri() . '/assets/img/circulo-depoimento.png'; ?>" class="circulo-depoimentos-depois d-none d-md-block"/>
</section>