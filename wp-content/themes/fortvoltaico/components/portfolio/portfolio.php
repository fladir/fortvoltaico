<?php
$args = array(
    'nopaging' => false,
    'post_type' => 'portfolio',
    'posts_per_page' => 8,
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);

$conteudoProtfolio = get_field('conteudo_portfolio');

?>

<section id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <h2 class="text-secundario text-center fw-bold mb-0"><?php echo $conteudoProtfolio['titulo'] ?></h2>
                <h4 class="text-white text-center fw-bold"><?php echo $conteudoProtfolio['subtitulo'] ?></h4>
            </div>
            <div class="owl-carousel owl-theme portfolio-carousel">
            <?php
            $i = 0;
            if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                <?php
                $galeria_de_imagens = get_field('galeria');
                $informacoes = get_field('informacoes');
                ?>

                <div class="item">
                    <a class="link-portfolio" data-fancybox="galeria<?php echo $i; ?>"
                       href="<?php echo get_the_post_thumbnail_url($post->ID, 'portfolio_big'); ?>">
                        <?php the_post_thumbnail('portfolio', array('class' => 'img-portfolio', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>

                        <div class="wrapper-info">
                            <p><strong>Potencia:</strong> <?php echo $informacoes['potencia']; ?></p>
                            <p><strong>Quantidade de placas:</strong> <?php echo $informacoes['quantidade_de_placas']; ?></p>
                            <p><strong>Economia anual:</strong> <?php echo $informacoes['economia_anual']; ?></p>
                            <p><strong>Recuperação do investimento:</strong> <?php echo $informacoes['recuperacao_do_investimento']; ?></p>
                        </div>
                    </a>

                    <?php foreach ($galeria_de_imagens as $image): ?>

                        <a data-fancybox="galeria<?php echo $i; ?>"
                           href="<?php print_r($image['sizes']['portfolio_big']) ?>" alt="<?php the_title() ?>"
                           style="display: none;">
                            <img src="<?php print_r($image['sizes']['portfolio_big']) ?>" alt="<?php the_title() ?>">
                        </a>

                    <?php endforeach; ?>

                </div>
                <?php
                $i++;
            endwhile;
            endif;
            wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>
