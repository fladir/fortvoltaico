<?php
$conteudoCalc = get_field('conteudo_calculadora');
?>

<section id="calculadora">
    <div class="container">
        <div class="row">
            <div class="col-md-5 calc-content bg-secundario p-5">
                <?php get_template_part('components/formularios/calculadora'); ?>
            </div>
            <div class="col-md-7">

            </div>
        </div>
    </div>
    <img src="<?php print_r($conteudoCalc['imagem']['sizes']['calculadora']) ?>"
         alt="Calcular" class="attachment-calc">

</section>
