<?php
$comoFunciona = get_field('como_funciona');
?>

<section id="como-funciona">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-primario text-center fw-bold mb-5"><?php echo get_field('titulo_como_funciona') ?></h2>
            </div>
            <?php if ($comoFunciona) : foreach ($comoFunciona as $item) : ?>
                <div class="col-md my-5">
                    <div class="wrapper-attachment-como-funciona">
                        <img src="<?php print_r($item['imagem']['sizes']['como_funciona']) ?>"
                             alt="<?php echo $item['titulo']; ?>" class="attachment-como-funciona">
                    </div>
                    <div class="wrapper-portfolio-content text-center">
                        <h4 class="text-center fw-bold"><?php echo $item['titulo']; ?></h4>
                        <?php echo $item['texto']; ?>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>
