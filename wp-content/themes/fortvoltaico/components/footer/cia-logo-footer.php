<?php
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais']
?>
<div class="footer-copyright text-center py-3 container">


            <div class="container footer-copyright-content">

                <div class="row">
                    <disev class="col-md-6 py-2 d-flex justify-content-start px-0">
                <span class="fw-regular">
                    <i class="far fa-copyright"></i> <?php echo date('Y'); ?> Todos os direitos reservados. <?php echo get_bloginfo( 'name' ); ?>
                </span>
                    </disev>

                    <div class="col-md-6 py-2 d-flex justify-content-end px-0">
                <a href="http://bit.ly/38DuDmd" target="_blank"> <img
                            src="<?php bloginfo('template_directory'); ?>/assets/img/logo-cia-branca.png" class="logo-cia"/></a>
                    </div>
                </div>

            </div>


</div>