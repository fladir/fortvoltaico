<?php
$botaoWhatsapp = get_fields('options')['grupo_informacoes_para_contato']['whatsapp'];
?>
<?php #echo'<pre>'; print_r($botaoWhatsapp); echo'</pre>'; ?>
<div id="navbarWapper">
    <nav id="menu-secundario" class="navbar navbar-expand-lg py-3" role="navigation">

        <div class="container">

            <a class="navbar-brand" href="<?php bloginfo('url'); ?>" title="">
                <?php echo wp_get_attachment_image(get_field('grupo_header', 'options')['logo_colorida'], 'logo_header'); ?>
            </a>

            <?php if (is_home() || is_front_page()) : ?>
                <h1 class="sr-only"><?php echo get_field('grupo_header', 'options')['nome_da_empresa'] ? get_field('grupo_header', 'options')['nome_da_empresa'] : get_bloginfo('name'); ?></h1>
            <?php endif; ?>

            <?php

            wp_nav_menu(array(
                'theme_location' => 'primary',
                'depth' => 3,
                'container' => 'div',
                'container_class' => 'navbar-collapse justify-content-end site-navbar',
                'container_id' => 'navbarNav',
                'menu_class' => 'nav navbar-nav navbar-desktop',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                'walker' => new WP_Bootstrap_Navwalker(),
            ));


            ?>

            <div id="navTogglerFixed" class="nav-toggler d-sm-block d-lg-none">
                <span></span>
            </div>
        </div>
        <div class="nav-toggler d-sm-block d-lg-none">
            <span></span>
        </div>
    </nav>
</div>
<?php wp_enqueue_script('headerNavPadraoJS', get_template_directory_uri() . '/components/header/scrollNavPadrao.js', array('jquery'), 1, true); ?>
<?php get_template_part('components/header/mobile_menu'); ?>

</div>
<div id="navTogglerFixed" class="nav-toggler d-sm-block d-lg-none">
    <span></span>
</div>




