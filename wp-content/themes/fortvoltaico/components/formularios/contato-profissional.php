<form class="bianca-form p-2 p-md-0" id="contato-profissional">
    <div class="row my-0">
        <h3 class="mt-0">Informações pessoais</h3>
        <div class="col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="nomeProfissional" class="sr-only">Nome</label>
                <input type="text" class="form-control" name="nome" id="nomeProfissional" placeholder="Nome*">
            </div>
        </div>

        <div class="form-group col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="aniversarioProfissional" class="sr-only">Aniversário</label>
                <input type="text" class="form-control date" name="aniversario" id="aniversarioProfissional" placeholder="Aniversário">
            </div>
        </div>

        <div class="col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="telProfissional" class="sr-only">Telefone Comercial</label>
                <input type="text" class="form-control phone" name="tel" id="telProfissional" placeholder="Telefone Comercial*">
            </div>
        </div>

        <div class="form-group col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="celularProfissional" class="sr-only">Celular</label>
                <input type="text" class="form-control cel" name="cel" id="celularProfissional" placeholder="Celular*">
            </div>
        </div>

        <div class="form-group col-12 px-1 my-1">
            <label for="email" class="sr-only">E-mail</label>
            <input type="email" class="form-control" name="email" id="emailProfissional" placeholder="E-mail*">
        </div>
    </div>

    <div class="row my-0">
        <h3>Localização</h3>
        <div class="form-group col-12 px-1 my-1">
            <label for="enderecoProfissional" class="sr-only">Endereço</label>
            <input type="text" class="form-control" name="endereco" id="enderecoProfissional" placeholder="Endereço*">
        </div>

        <div class="form-group col-12 px-1 my-1">
            <label for="bairroProfissional" class="sr-only">Bairro</label>
            <input type="text" class="form-control" name="bairro" id="bairroProfissional" placeholder="Bairro*">
        </div>

        <div class="col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="cidadeContato" class="sr-only">Cidade</label>
                <input type="text" class="form-control" name="cidade" id="cidadeProfissional" placeholder="Cidade*">
            </div>
        </div>

        <div class="form-group col-12 col-md-6 px-1 mb-0">
            <div class="form-group mb-1 pb-0">
                <label for="estadoProfissional" class="sr-only">Estado</label>
                <input type="text" class="form-control" name="estado" id="estadoProfissional" placeholder="Estado*">
            </div>
        </div>
    </div>

    <div class="row my-0">
        <h3>Sobre o projeto</h3>
        <div class="form-group col-12 px-1 my-1">
            <label for="atividadeProfissional" class="sr-only">Atividade profissional</label>
            <input type="text" class="form-control" name="atividade" id="atividadeProfissional" placeholder="Atividade profissional*">
        </div>
        <div class="col-12 p-1">
            <label for="mensagemAtividade" class="sr-only">Mensagem</label>
            <textarea class="form-control" name="mensagemAtividade" id="mensagemAtividade" rows="3" placeholder="Mensagem*"></textarea>
        </div>
        
    </div>

    <div class="row my-0">
        <h3>Dados bancários</h3>
        <div class="form-group col-12 px-1 my-1">
            <label for="contaProfissional" class="sr-only">Cadastrar dados</label>
            <input type="text" class="form-control" name="conta" id="contaProfissional" placeholder="Cadastrar dados">
        </div>
        
    </div>

    <div class="row my-0">
        <h3>Observações</h3>
        <div class="col-12 p-1">
            <label for="mensagem" class="sr-only">Mensagem</label>
            <textarea class="form-control" name="mensagem" id="mensagemAdicional" rows="3" placeholder="Mensagem"></textarea>
        </div>
        <div class="col-12 p-1">
            <div class="form-group mb-0">
                <button type="button" class="btn btnFormBianca" id="btnSubmit" onclick="enviarMensagem()">Enviar mensagem</button>
            </div>
        </div>
    </div>
</form>

<?php wp_enqueue_script('Form Profissional', get_template_directory_uri() . '/components/formularios/contato-profissional.js', array('jquery'), 1, true);