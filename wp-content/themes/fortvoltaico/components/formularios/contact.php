<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
    'nopaging' => false,
    'paged' => $paged,
    'post_type' => 'tour',
    'order' => 'ASC',
    'posts_per_page' => 100
);
$WPQuery = new WP_Query($args);
$formulario = get_field('conteudo_do_banner')['formulario'];
?>

<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-2">
        <div class="form-header header-primary"></div><!-- end .form-header section -->
        <div class="form-contact-wrap m-t-40">

            <form class="xs-form" method="post" action="<?php bloginfo('url') ?>/resposta/" id="smart-form"
                  enctype="multipart/form-data">
                <input type="hidden" name="formulario" value="<?php echo the_title(); ?>">
                <div class="form-body">
                    <?php /* INICIO: NÃO REMOVER OU ALTERAR OS CAMPOS \/ */ ?>
                    <div class="section" style="display:none">
                        <label class="field prepend-icon">
                            <input type="text" name="formulario" id="formulario"
                                   value="<?php echo str_replace('/', '', basename(get_permalink())); ?>"
                                   class="gui-input nome">
                            <b><i class="fa fa-cogs" aria-hidden="true"></i> Página do formulário</b>
                        </label>
                    </div>
                    <div class="section" style="display:none">
                        <label class="field prepend-icon">
                            <input type="text" name="baseurl" id="baseurl" value="<?php echo get_site_url(); ?>"
                                   class="gui-input nome">
                            <b><i class="fa fa-cogs" aria-hidden="true"></i> BASE URL</b>
                        </label>
                    </div>
                    <?php /* FINAL: NÃO REMOVER OU ALTERAR OS CAMPOS /\ */ ?>
                    <div class="row">
                        <div class="col-12">
                            <input required type="text" name="nome" id="nome" class="form-control mb-4"
                                   placeholder="Nome">
                        </div>
                        <div class="col-12">
                            <input required type="text" name="tel" id="tel" class="form-control mb-4"
                                   placeholder="Telefone">
                        </div>
                        <div class="col-12">
                            <input required type="email" name="email" id="email" class="form-control invaild mb-4"
                                   placeholder="E-mail">

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-12">
                            <textarea required class="form-control message-box mb-4" id="mensagem" name="mensagem" cols="30" rows="4" placeholder="Mensagem"></textarea>
                        </div>
                    </div>

                    <div class="button-captcha">
                        <div class="section cap">
                            <div class="captcha">
                                <div class="row ">
                                    <div class="col-6 pr-2">
                                        <input required type="text" name="securitycode" id="securitycode"
                                               class="gui-input sfcode form-control" placeholder="Digite o Código">
                                    </div>
                                    <div class="col-6 pl-2">
                                        <div class="button captcode">
                                            <img src="<?php bloginfo('template_url') ?>/components/formularios/captcha/captcha.php"
                                                 id="captcha" alt="Captcha"/>
                                        </div>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <button name="LetheForm_Save" type="submit" class="w-100 mt-4 btn form fw-bold">
                                            <?php echo $formulario['texto_do_botao'] ? $formulario['texto_do_botao'] : 'Enviar'?>
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="result"></div><!-- end .result  section -->
                </div>
                <div class="msgs-formulario">
                </div>
            </form>

        </div>
    </div>
</div>
<?php wp_enqueue_script('jquery-validate', get_bloginfo('template_url') . '/node_modules/jquery-validation/dist/jquery.validate.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-validate-translate', get_bloginfo('template_url') . '/assets/js/messages_pt_BR.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-maskedinput', get_bloginfo('template_url') . '/node_modules/jquery.maskedinput/src/jquery.maskedinput.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery_form', get_bloginfo('template_url') . '/assets/js/captcha/jquery.form.min.js', array('jquery')) ?>
<?php wp_enqueue_script('smart_form', get_bloginfo('template_url') . '/assets/js/captcha/smart-form.js', array('jquery')) ?>
<?php wp_enqueue_script('sweetAlert', get_bloginfo('template_url') . '/node_modules/sweetalert/dist/sweetalert.min.js', array('jquery')) ?>
