<?php
$grupoCtaEForm = get_field('grupo_call_to_action_com_formulario')
?>
<div class="row">
    <div class="col-12">
        <form id="formularioContato" class="form-signin p-0 m-0" method="POST" onsubmit="return false">
            <div class="form-label-group mb-3">
                <input type="nome" id="nomeContato" class="form-control" placeholder="Nome Completo" name="nome"
                       required autofocus>
            </div>

            <div class="form-label-group mb-3">
                <input type="text" id="telefoneContato" class="form-control" placeholder="Telefone" name="telefone"
                       required autofocus>
            </div>

            <div class="form-label-group mb-3">
                <input type="email" id="emailContato" class="form-control" placeholder="Email" name="email" required
                       autofocus>
            </div>

            <div class="form-label-group mb-3">
                <textarea class="form-control" id="mensagemContato" placeholder="Sua mensagem" name="mensagem" required
                          autofocus rows="4"></textarea>
            </div>

            <div class="form-label-group">
                <div class="row">
                    <div class="col-6 pr-2">
                        <input type="text" class="form-control lgui-input sfcode" placeholder="Digite o Código"
                               name="securitycode" id="securitycode" required autofocus>
                    </div>
                    <div class="col-6 pl-2">
                        <div class="button captcode">
                            <img src="<?php bloginfo('template_url') ?>/components/formularios/captcha/captcha.php"
                                 id="captcha"
                                 alt="Captcha"/>
                            <span class="refresh-captcha"><i class="fas fa-sync-alt"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="button_su mt-4 w-100">
                <span class="su_button_circle"></span>
                <button id="btnSubmit" type="submit" onclick="enviarMensagem()" class="btn button_su_inner primario w-100">
                    <span class="button_text_container"><?php echo $grupoCtaEForm['texto_botao_formulario']; ?></span>
                </button>
            </div>
        </form>
    </div>
</div>
<?php wp_enqueue_script('Form Profissional', get_template_directory_uri() . '/components/formularios/contato-profissional.js', array('jquery'), 1, true); ?>
<?php wp_enqueue_script('jquery-validate', get_bloginfo('template_url') . '/node_modules/jquery-validation/dist/jquery.validate.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-validate-translate', get_bloginfo('template_url') . '/assets/js/messages_pt_BR.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-maskedinput', get_bloginfo('template_url') . '/node_modules/jquery.maskedinput/src/jquery.maskedinput.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery_form', get_bloginfo('template_url') . '/assets/js/captcha/jquery.form.min.js', array('jquery')) ?>
<?php wp_enqueue_script('smart_form', get_bloginfo('template_url') . '/assets/js/captcha/smart-form.js', array('jquery')) ?>