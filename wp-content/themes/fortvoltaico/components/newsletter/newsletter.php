<section id="newsletter">

    <div class="container">

        <div class="row px-5">
            <div class="col-md-12 my-5">
            <h2 class="text-left titulo-saindo-secundario mb-4"
            >Newsletter</h2>
            </div>
            <div class="col-12 frase-newsletter mb-4">
                <h3 class="text-white">Cadastre-se em nossa newsletter e acompanhe nossas novidades</h3>
            </div>
            <form class="form-newsletter form-inline w-100" method="get" >
            <div class="col-12 col-md-5 px-2 mb-0">
                <div class="form-group mb-1 pb-0">
                    <input type="text" name="nome" value="" size="40" placeholder="Nome">
                </div>
            </div>

            <div class="form-group col-12 col-md-5 px-2 mb-0">
                <div class="form-group mb-1 pb-0 w-100">
                    <input type="email" name="email" value="" size="40" placeholder="E-mail">
                </div>
            </div>
            <div class="form-group col-12 col-md-2 px-2 mb-0">
                <div class="form-group mb-1 pb-0 w-100">
                    <div class="button_su secundario w-100">
                        <span class="su_button_circle"></span>
                        <button style="width: 100%" type="submit" class="btn button_su_inner primario">
                            <span class="button_text_container">Cadastrar</span>
                        </button>
                    </div>
                </div>
            </div>
            </form>
        </div>



    </div>

</section>

<?php /* Restore original Post Data */
wp_reset_postdata(); ?>