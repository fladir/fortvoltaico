<?php
$conteudoChamada = get_field('conteudo_da_chamada');
#echo '<pre>'; print_r($conteudoChamada); echo '</pre>';
?>

<section id="chamada">
    <div class="container">
        <div class="row">
            <div class="col-md-7 pr-md-5">
                <h2 class="text-secundario fw-bold mb-4">
                    <?php echo $conteudoChamada['titulo'] ?>
                </h2>

                <?php echo $conteudoChamada['texto'] ?>

            </div>
            <div class="col-md-5">
                <img src="<?php print_r($conteudoChamada['icone']['sizes']['ic_chamada']) ?>" alt="<?php echo $conteudoChamada['titulo'] ?>" class="attachment-chamada">
            </div>
        </div>
    </div>
</section>
