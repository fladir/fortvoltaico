<?php
$conteudoBanner = get_field('conteudo_do_banner');
$circuloDestaque = $conteudoBanner['circulo_destaque'];
$formulario = $conteudoBanner['formulario'];
#echo '<pre>'; print_r($formulario); echo '<?pre>';
?>
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="fw-bold text-white px-4"><?php echo $conteudoBanner['texto_principal'] ?></h1>
                <div class="circulo-destaque d-flex justify-content-center flex-column">
                    <span class="text-antes fw-bold text-white"><?php echo $circuloDestaque['texto_antes'] ?></span>
                    <h3 class="porcentagem text-secundario fw-bold"><?php echo $circuloDestaque['porcentagem'] ?><span class="fw-black">%</span></h3>
                    <span class="text-depois fw-bold text-white"><?php echo $circuloDestaque['texto_depois'] ?></span>
                </div>
                <img src="<?php print_r($conteudoBanner['imagem_destaque']['sizes']['banner']) ?>" alt="<?php echo $conteudoBanner['texto_principal'] ?>" class="attachment-banner">

            </div>
            <div class="col-md-5 weight">
                <div class="wrapper-formulario">

                    <h4 class="text-primario fw-black px-4 mb-5"><?php echo $formulario['titulo'] ?></h4>
                    <?php get_template_part('components/formularios/contact'); ?>

                </div>
            </div>
        </div>
    </div>
</section>