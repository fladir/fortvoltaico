<?php
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
    add_image_size('logo_header', 360, 64); //Logo Header
    add_image_size('banner', 1470, 512); // Imagem Banner
    add_image_size('imagens_col6', 960, 590, true); // Imagens metade da página
    add_image_size('diferenciais', 162, 145); // Ícones Diferenciais
    add_image_size('ic_chamada', 790, 790); // Ícones Chamada
    add_image_size('secoes_alternadas', 485, 350); // Seções Alternadas
    add_image_size('footer_logo', 374, 68); // Footer Logo
    add_image_size('portfolio', 360, 360, true); // Portfólio
    add_image_size('portfolio_big', 800, 600, true); // Portfólio Big
    add_image_size('como_funciona', 410, 297); // Como Funciona
    add_image_size('depoimentos', 150, 150, true); // Depoimentos
    add_image_size('calculadora', 914, 626); // Calculadora


}