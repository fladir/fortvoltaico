<?php

// Register widget
add_action('widgets_init', 'banner_widget');
function banner_widget()
{
    register_widget('bannerWidget');
}

// Enqueue additional admin scripts
add_action('admin_enqueue_scripts', 'banner_script');
function banner_script()
{
    wp_enqueue_media();
    wp_enqueue_script('ads_script', get_template_directory_uri() . '/core/assets/js/admin-scripts.js', false, '1.0.0', true);
}

// Widget
class bannerWidget extends WP_Widget
{


    function __construct()
    {
        parent::__construct(

// Base ID of your widget
            'banner_widget',

// Widget name will appear in UI
            __('CTA Blog', 'banner_widget_domain'),

// Widget description
            array('description' => __('Banner CTA a ser exibido na sidebar do blog', 'banner_widget_domain'),)
        );
    }


    function widget($args, $instance)
    {
        echo $before_widget;
        ?>
        <div class="cta-blog-widget card border-0 mb-5">
            <div class="conteudo-cta-blog-widget">
                <h4><?php echo apply_filters('widget_title', $instance['frase']); ?></h4>
                <div class="wrapper-seta">
                    <img class="seta-cta-blog" src="<?php echo get_template_directory_uri() . '/assets/img/seta-cta.png'; ?>" alt="">
                </div>
                <div class="button_su secundario w-100">
                    <span class="su_button_circle"></span>
                    <a href="<?php echo apply_filters('widget_title', $instance['link_botao']); ?>"
                       class="btn button_su_inner primario btn-cta-blog">
                        <span class="button_text_container"><?php echo apply_filters('widget_title', $instance['texto_botao']); ?></span>
                    </a>
                </div>
            </div>
        </div>
        <?php
        echo $after_widget;

    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['frase'] = strip_tags($new_instance['frase']);
        $instance['texto_botao'] = strip_tags($new_instance['texto_botao']);
        $instance['link_botao'] = strip_tags($new_instance['link_botao']);
        return $instance;
    }

    function form($instance)
    {
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('frase'); ?>">Frase de efeito (Título)</label><br/>
            <input type="text" name="<?php echo $this->get_field_name('frase'); ?>"
                   id="<?php echo $this->get_field_id('frase'); ?>" value="<?php echo $instance['frase']; ?>"
                   class="widefat"/><br><br>
            <label for="<?php echo $this->get_field_id('texto_botao'); ?>">Texto do botão</label><br/>
            <input type="text" name="<?php echo $this->get_field_name('texto_botao'); ?>"
                   id="<?php echo $this->get_field_id('texto_botao'); ?>"
                   value="<?php echo $instance['texto_botao']; ?>"
                   class="widefat"/><br><br>
            <label for="<?php echo $this->get_field_id('texto_botao'); ?>">Link do botão</label><br/>
            <input type="text" name="<?php echo $this->get_field_name('link_botao'); ?>"
                   id="<?php echo $this->get_field_id('link_botao'); ?>" value="<?php echo $instance['link_botao']; ?>"
                   class="widefat"/>
        </p>


        <?php
    }
}