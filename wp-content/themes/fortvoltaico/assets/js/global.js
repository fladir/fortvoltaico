(function ($) {

    $(document).ready(function () {
        $('.menu-item-244 .nav-link').on('click', function (e) {
            e.preventDefault();
        });

        $('.menu-item-244 .nav-link').attr('data-toggle', 'modal' ).attr('data-target', '#exampleModal' );

        $('[data-fancybox]').fancybox({
            autoStart: true,
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#voltarTopo').addClass('show')
            } else {
                $('#voltarTopo').removeClass('show');
            }
        });
        $('#voltarTopo').click(function () {
            $("html, body").animate({scrollTop: 0}, 1200, 'easeInOutExpo');
            return false;
        });

        // Carousel Portfolio
        $(".portfolio-carousel").owlCarousel({
            loop: true,
            items: 3,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 1000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                }
            }
        });

        // Carousel Depoimentos
        $(".depoimento-carousel").owlCarousel({
            loop: true,
            items: 1,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 500,
            slideBy: 1,
            margin: 25,
            autoplayHoverPause: true
        });

        //WOW Effect
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        );
        wow.init();
    });

    //Botão Menu Mobile
    $('.nav-toggler, .mobile-menu-overlay').click(function () {
        $('.nav-toggler').toggleClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
    });
    $('.nav-link').click(function () {
        $('.nav-toggler').removeClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').removeClass('open');
    });

    //Contagem Nossos Números
    $('.nav-link').removeAttr('title');

})(jQuery);