$(function() {
    function reloadCaptcha(){
        let url = $("#captcha").attr("src");
        url = url.split('?');
        $("#captcha").attr("src",url[0]+"?r=" + Math.random());
    }
    $("#tel").mask("(99) 9999-9999?9");
    /*var behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options);
        }
    };
    
    $('#tel').mask(behavior, options);*/
    $('.captcode').click(function(e){
        e.preventDefault();
        reloadCaptcha();
    });


    $('.data-de-nascimento').mask('00/00/0000');

    $('#smart-form').submit(function(e) {
        e.preventDefault();
        let baseurl = $("#baseurl").val();
        $('.msgs-formulario').html('');
        $('.msgs-formulario').html('<div class="alerta alerta-loading"> <i class="fa fa-spinner fa-pulse"></i> AGUARDE!<br></div>');
        var data = new FormData();
        $.each($('#smart-form').find("input, textarea, select"), function (key, field) {
            var name = $(field).attr("name");
            var type = $(field).attr("type");
            var value = "";
            if (type == "file") {
                if (field.files && field.files.length) {
                    $.each(field.files, function (i, file) {
                        data.append(name, file);
                    });
                }
            } else if (type == "checkbox") {
                if (field.checked)
                    data.append(name, $(field).val());
            } else if (type == "radio") {
                if (field.checked)
                    data.append(name, $(field).val());
            } else {
                data.append(name, $(field).val());
            }
        });
        console.log("testing");
        
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log("sucesso");
                $('.msgs-formulario').html('<div class="alerta alerta-sucesso">Mensagem enviada com sucesso! Responderemos o mais breve possível</div>');                
            },
            error: function(data) {
                console.log("caiu em erro");
                $('.msgs-formulario').html('<div class="alerta alerta-erro">Informações incompletas ou erradas. Tente novamente<br></div>');
            },
            complete: function(data) {
                console.log(data);
                reloadCaptcha();
            }
        })
    });



    /* ini: Requisicao de prontuario */


    
    /* end: Requisicao de prontuario */        
});